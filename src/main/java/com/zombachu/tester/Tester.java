package com.zombachu.tester;

import org.bukkit.plugin.java.JavaPlugin;

public class Tester extends JavaPlugin {

    @Override
    public void onEnable() {
        getCommand("serialize").setExecutor(new SerializeCommand());
        getCommand("deserialize").setExecutor(new DeserializeCommand());
    }
}
