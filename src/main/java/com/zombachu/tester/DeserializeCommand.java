package com.zombachu.tester;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class DeserializeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You need to be a player to use this command.");
            return true;
        }

        Player player = (Player) sender;
        File file = new File(Tester.getPlugin(Tester.class).getDataFolder(), player.getUniqueId().toString() + ".yml");

        if (!file.exists()) {
            player.sendMessage("You need to run /serialize before using /deserialize.");
            return true;
        }

        YamlConfiguration yaml = new YamlConfiguration();

        try {
            yaml.load(file);
        } catch (IOException|InvalidConfigurationException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 36; i++) {
            if (yaml.contains("" + i)) {
                player.getInventory().setItem(i, yaml.getItemStack("" + i));
            }
        }

        sender.sendMessage("Loaded your saved inventory into your current inventory.");
        return true;
    }
}
