package com.zombachu.tester;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;

public class SerializeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You need to be a player to use this command.");
            return true;
        }

        Player player = (Player) sender;
        YamlConfiguration yaml = new YamlConfiguration();

        for (int i = 0; i < 36; i++) {
            ItemStack item = player.getInventory().getItem(i);

            if (item != null && !item.getType().equals(Material.AIR)) {
                yaml.set("" + i, item);
            }
        }

        File file = new File(Tester.getPlugin(Tester.class).getDataFolder(), player.getUniqueId().toString() + ".yml");
        try {
            yaml.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sender.sendMessage("Saved your current inventory.");
        return false;
    }
}
